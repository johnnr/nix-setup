# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.plymouth.enable = true;

  environment.etc."xdg/user-dirs.defaults".text = ''
    DESKTOP=
    DOWNLOAD=dl
    TEMPLATES=dox/templates
    DOCUMENTS=dox
    MUSIC=music
    PICTURES=pix
    VIDEOS=vids
    PUBLICSHARE=dox/public
  '';

  networking.hostName = "nix01";
  #networking.wireless.iwd.enable = true;
  networking.networkmanager.enable = true;
  networking.useDHCP = false;
  #networking.interfaces.wlan0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;
  networking.extraHosts = "45.76.117.20 vps01";
  services.openssh.enable = true;

  i18n.defaultLocale = "en_AU.UTF-8";
  i18n.inputMethod = {
    enabled = "fcitx";
    fcitx.engines = with pkgs.fcitx-engines; [ mozc ];
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  hardware.cpu.intel.updateMicrocode = true;

  # Set your time zone.
  time.timeZone = "Australia/Melbourne";

  nixpkgs.config.allowUnfree = true;

  fonts.fonts = with pkgs; [
   
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    inconsolata
    iosevka
    siji
    cantarell-fonts
    unifont

  ];

  environment.systemPackages = with pkgs; [

    # Background noise.
    sxhkd  gnupg
    dunst  libnotify
    openssl
    xdg-user-dirs
    
    # User level stuff.
    neovim vim
    maim   firefox
    sxiv   alacritty
    wget   curl
    git    udisks2
    xcwd   qbittorrent
    xdo    libreoffice
    dmenu  chromium
    feh    pavucontrol
    fzf    discord
    mpv    zathura
    ffmpeg youtube-dl
    arandr fcitx-configtool
    pandoc spotify
    acpi   thunderbird
    htop   gtypist
    ranger polybar
    woeusb killall
    gimp   lutris
    #typora teamspeak_client
    sc-im 

    # Programming stuff.
    ghc    docker-compose
    gcc    python38
    texlive.combined.scheme-full

  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "gnome3";
  };
  programs.zsh.enable = true;
  programs.nm-applet.enable = true;

  security.sudo.wheelNeedsPassword = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.fstrim.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";

  services.xserver.desktopManager.plasma5.enable = true;

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
  services.xserver.libinput.naturalScrolling = true;

  services.xserver.windowManager.bspwm.enable = true;
  #services.xserver.displayManager.startx.enable = true;
  
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.john = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" "networkmanager" ];
    shell = pkgs.zsh;
  };

  virtualisation.docker.enable = true;
  virtualisation.docker.enableOnBoot = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

}

