#!/bin/sh

# Import zshrc if it exists.
[ -f $HOME/.zshrc ] && . $HOME/.zshrc

# Configuration directories and shit.
#export XDG_DESKTOP_DIR="$HOME"
#export XDG_DOCUMENTS_DIR="$HOME/dox"
#export XDG_DOWNLOAD_DIR="$HOME/dl"
#export XDG_PICTURES_DIR="$HOME/pix"
export XDG_CONFIG_HOME="$HOME/.config"
export TERMINFO="/usr/share/terminfo"

# Delete dis shit.
export LESSHISTFILE="-"

# Default programs.
export BROWSER="chromium"
export EDITOR="nvim"
export TERMINAL="alacritty"

# IM module variables.
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

# Adding stuff to path.
[ -d $HOME/.scripts ] && export PATH=$HOME/.scripts:$PATH

# Autostart xorg.
[ "$(tty)" = "/dev/tty1" ] && startx
