# Lines configured by zsh-newuser-install
HISTFILE=~/.logs/ZSH_HISTFILE
HISTSIZE=1000000000
SAVEHIST=1000000000
setopt beep extendedglob
setopt +o nomatch
setopt autocd
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/john/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# User aliases and functions.
[ -f $HOME/.zaliases ] && source $HOME/.zaliases

# fzf.
[ -n "${commands[fzf-share]}" ] && source "$(fzf-share)/key-bindings.zsh" && source "$(fzf-share)/completion.zsh"

# PS1 for prompt.
export PS1="%F{14}%n%f@%F{220}%m%f%F{213}>%f "
