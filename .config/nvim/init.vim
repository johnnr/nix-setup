call plug#begin()

Plug 'junegunn/goyo.vim'

call plug#end()

" Add line numbers.
set number relativenumber

" Setting tab to equal two spaces.
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

" Status bar.
set laststatus=2

" Automatically reload sxhkd on change to configuration.
autocmd BufWritePost sxhkdrc :silent !pkill -USR1 -x sxhkd

" Automatically recompile latex.
autocmd BufWritePost *.tex :silent !xelatex -interaction=nonstopmode %
